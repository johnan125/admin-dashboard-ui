export const environment = {
  production: true,
  loginEndpoint: 'https://43d8ym1ii6.execute-api.us-east-2.amazonaws.com/dev/login/authenticate',
  profileEndpoint: 'https://43d8ym1ii6.execute-api.us-east-2.amazonaws.com/dev/profile'
};
