import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  name:string = "this is working";

  constructor(public authenticationService: AuthenticationService, private router:Router) { }

  ngOnInit() {
    console.log("The value of name is " + this.name)
    this.authenticationService.userMessage$.subscribe(message => {
      console.log("Subscribed");
      this.name = message;
    });
  }


  logout(){
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
