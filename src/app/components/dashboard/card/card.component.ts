import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Card } from 'src/app/model/Card';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() card: Card;
  constructor() { }

  ngOnInit() {

  }

  cardClass() {
    if(this.card.name === "Earnings"){
      return {
        'border-left-success': true
      }
    }else if(this.card.name === "Losses"){
      return {
        'border-left-danger': true
      }
    }else {
      return {
        'border-left-primary': true
      }
    }
    
  }


}
