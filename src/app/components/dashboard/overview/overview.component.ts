import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { CovidService } from 'src/app/services/covid.service';
import { TotalCountry } from 'src/app/model/TotalCountry';
declare function areaChart(name): any;
declare function areaChart(name, data): any;
@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  type:string;
  data:any;

  constructor(private route: ActivatedRoute, private covidService: CovidService) { }

  ngOnInit() {
    this.type = this.route.snapshot.paramMap.get('type');
    this.covidService.getListOfMonthlyCasesByType(this.type).subscribe((data: Array<TotalCountry>) => {
      this.data = this.covidService.formatConfirmedCasesByMonth(data);
      areaChart(this.type, this.data);
    });
  }

}
