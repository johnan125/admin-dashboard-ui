import { Component, OnInit, Input } from '@angular/core';
import { Card } from 'src/app/model/Card';
import { CovidService } from 'src/app/services/covid.service';
declare function areaChart(name): any;
@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  @Input() card:Card;
  data:any;

  constructor(private covidService: CovidService) { }

  ngOnInit() {
  }

  ngAfterViewInit(){
    console.log(this.card.name);
    areaChart(this.card.name);
  }

}
