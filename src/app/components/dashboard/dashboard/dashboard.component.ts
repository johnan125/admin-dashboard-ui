import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { Card } from 'src/app/model/Card';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { WorldCovidCases } from 'src/app/model/WorldCovidCases';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  showSpinner: boolean = false;

  constructor(private dashboardService: DashboardService, private router: Router, private authenticationService: AuthenticationService, private r: ActivatedRoute) { }

  public cardData: Card[];

  ngOnInit() {
    console.log("DASHBOARD LOADED!");
    this.showSpinner = true;
    if (this.authenticationService.isUserLoggedIn()) {
      this.dashboardService.loadCardDataForUser().subscribe((data: WorldCovidCases) => {
          this.cardData = [{
            name: "Total World Confirmed Cases",
            link: "Cases",
            value: data.TotalConfirmed
          },
          {
            name: "Total World Confirmed Deaths ",
            link: "Deaths",
            value: data.TotalDeaths
          },
          {
            name: "Total World Confirmed Recoveries",
            link: "Recoveries",
            value: data.TotalRecovered
          },
          {
            name: "Requests",
            link: "reqeests",
            value: "1"
          }];
          this.showSpinner = false;
          localStorage.setItem("WorldCovidCases", data.toString());
          console.log("LOCAL STORAGE : " + localStorage.getItem("WorldCovidCases"));
        }

      );
    } else {
      this.cardData = this.dashboardService.loadCardData();
    }

  }

  clickedRow(card) {
    this.router.navigate(['overview', card.link], { relativeTo: this.r });
  }

  generateReport(){
    
  }
}
