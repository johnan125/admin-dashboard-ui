import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { User } from 'src/app/model/User';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  showSpinner: boolean = true;
  user: {
    name: "",
    email: ""
  };

  constructor(private authenticationService: AuthenticationService,
            private dashboardService: DashboardService) { }
  
  ngOnInit() {
    setTimeout(()=>{
      this.showSpinner = false;
    }, 5000);
    
    this.authenticationService.userMessage$.subscribe(x=> {
      console.log("value: " + JSON.parse(sessionStorage.getItem("userData")));
      this.user = JSON.parse(sessionStorage.getItem("userData"));
      console.log(this.user);
    })

    this.dashboardService.loadProfile(this.user.email).subscribe(data => {
      console.log("PROFILE DATA: " + data);
    },
    
    (error)=>{
      console.log(error);
    })
    
  }

}
