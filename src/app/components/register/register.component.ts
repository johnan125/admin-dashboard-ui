import { Component, OnInit } from '@angular/core';
import { User } from '../../model/User';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  firstName:string;
  lastName:string;
  email:string;
  password:string;
  repeatPassword:string;

  constructor(private authenticationService:AuthenticationService, private router:Router) { }

  ngOnInit() {
    console.log(this.authenticationService.users)
  }

  registerAccount(){
    let newUser:User = {
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      password: this.password,
      repeatPassword: this.repeatPassword
    }

    this.authenticationService.addRegisteredUser(newUser);
    this.router.navigate(['/login']);
    console.log(this.authenticationService.users);
  }

}
