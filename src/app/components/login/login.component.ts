import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  error:boolean;
  showSpinner: boolean = false;

  constructor(private authenticationService: AuthenticationService,private router:Router) { }

  ngOnInit() {
    console.log("LOGIN PAGE LOAD");
    if(this.authenticationService.isUserLoggedIn()){
      this.router.navigate(['/dashboard']);
    }
    this.error = false;
  }

  login(){
    this.showSpinner = true;
    // setTimeout(()=>{
    //   let authenticated = this.authenticationService.authenticate(this.email, this.password);
    //   if(authenticated === true){
    //     this.showSpinner = false;
    //     console.log("Successfully logged in... Redirecting to dashboard");
    //     this.router.navigate(['/dashboard']);
    //   }else{
    //     console.log("Failed to log in. Please try again");
    //     this.error = true;
    //     this.showSpinner = false;
    //   }
    // }, 5000);
    this.authenticationService.authenticate(this.email, this.password).subscribe(x=> {
    
      console.log("Login Data: " + x);
    this.showSpinner = false;
    this.router.navigate(['/dashboard']);
    },
    (error)=>{
      this.showSpinner = false;
      console.log("Failed to log in. Please try again");
      this.error = true;
    }
    );
    console.log("DONE?")

  }
}
