export interface WorldCovidCases {

    TotalConfirmed:number;
    TotalDeaths:number;
    TotalRecovered:number;

}