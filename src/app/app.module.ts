import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { DashboardComponent } from './components/dashboard/dashboard/dashboard.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { CardComponent } from './components/dashboard/card/card.component';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ErrorComponent } from './components/error/error.component';
import { RegisterComponent } from './components/register/register.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { OverviewComponent } from './components/dashboard/overview/overview.component';
import { ChartsModule } from 'ng2-charts';
import { ChartComponent } from './components/dashboard/chart/chart.component';
import { AuthHttpInterceptorService } from './services/auth-http-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    TopbarComponent,
    FooterComponent,
    DashboardComponent,
    CardComponent,
    LoginComponent,
    ErrorComponent,
    RegisterComponent,
    ProfileComponent,
    SpinnerComponent,
    OverviewComponent,
    ChartComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ChartsModule
  ],
  providers: [
    {  
      provide:HTTP_INTERCEPTORS, useClass: AuthHttpInterceptorService, multi:true 
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
