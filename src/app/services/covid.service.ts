import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TotalCountry } from '../model/TotalCountry';

@Injectable({
  providedIn: 'root'
})
export class CovidService {

  readonly CONFIRMED_CASES_URL = "https://api.covid19api.com/total/country/united-states/status/confirmed?from=2020-03-01T00:00:00Z&to=2020-09-10T00:00:00Z";
  readonly CONFIRMED_DEATHS_URL = "https://api.covid19api.com/total/country/united-states/status/deaths?from=2020-03-01T00:00:00Z&to=2020-09-10T00:00:00Z";
  readonly CONFIRMED_RECOVERED_URL = "https://api.covid19api.com/total/country/united-states/status/recovered?from=2020-03-01T00:00:00Z&to=2020-09-10T00:00:00Z";

  constructor(private httpClient:HttpClient) {

   }

   getListOfMonthlyCasesByType(type:string): Observable<any>{
    let url = "";  
    console.log(type);
    switch(type){
        case 'Cases':
          url = this.CONFIRMED_CASES_URL;
          break;
        case 'Deaths':
          url = this.CONFIRMED_DEATHS_URL;
          break;
        case 'Recoveries':
          url = this.CONFIRMED_RECOVERED_URL;
          break;
        default:
          url = "";
          break;
      }
      return this.httpClient.get(url);
   }

   formatConfirmedCasesByMonth(data:Array<TotalCountry>){
    let found = new Set();
    data = data.filter(x=>{
      let date = new Date(x.Date).getMonth();
      console.log("DATE : " + date);
      if(found.has(date)){
        return false;
      }else{
        found.add(date);
        return true;
      }
    });
    console.log(data);
    return data;
   }
}
