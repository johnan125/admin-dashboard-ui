import { Injectable } from '@angular/core';
import { Card } from '../model/Card';
import { HttpClient } from '@angular/common/http';
import { WorldCovidCases } from '../model/WorldCovidCases';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private defaultCard: Array<Card> = [{
    name: "Earnings",
    link: 'Confirmed',
    value: Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(69000)
  },
  {
    name: "Losses",
    link: 'Confirmed',
    value: Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(49000)
  },
  {
    name: "Assets",
    link: 'Confirmed',
    value: Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(230000)
  },
  {
    name: "Requests",
    link: 'Confirmed',
    value: "14"
  }
  ];

  constructor(private httpClient:HttpClient) { }

  loadCardData(): Card[] {


    return this.defaultCard;
  }

  loadCardDataForUser() : Observable<any>{

      return this.httpClient.get('https://api.covid19api.com/world/total');
    
  }

  loadProfile(email: string): Observable<any> {
    return this.httpClient.post(environment.profileEndpoint, email);
  }
}
