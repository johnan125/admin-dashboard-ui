import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../model/User';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private _userMessageSource = new BehaviorSubject<string>(sessionStorage.getItem("username"));
  userMessage$ = this._userMessageSource.asObservable();

  public users:User[] = [{firstName: "john",
    lastName:"test",
    email:"test@gmail.com",
    password:"123",
    repeatPassword:"123"}];


  constructor(private httpClient:HttpClient) { }

  
  authenticate(email:string, password:string):Observable<any>{
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(email + ':' + password) });
    console.log("Username : " + email + " passsword : " + password);
    return this.httpClient.get(environment.loginEndpoint,{headers}).pipe(
     map(
       (userData:any) => {
        let authString = 'Basic ' + btoa(email + ':' + password);
        sessionStorage.setItem('username',email);
        sessionStorage.setItem('basicauth', authString);
        //placeholder for now...
        sessionStorage.setItem('userData', JSON.stringify(userData));
        console.log("Logging in with userData : " + userData);
        this._userMessageSource.next(userData.name);
        return userData;
       }
     )
    );
    // let user = this.findUser(email);
    // console.log("Logging in as user: " + user)
    // if(user !== undefined){
    //   sessionStorage.setItem('username', user.firstName);
    //   sessionStorage.setItem('basicauth', "epic");
    //   sessionStorage.setItem('userData', JSON.stringify(user));
    //   console.log("adding message to subject");
    //   this._userMessageSource.next(user.firstName);
    //   return true;
    // }else{
    //   return false;
    // }
  }

  isUserLoggedIn(){
    if(sessionStorage.getItem("username") !== null){
      return true;
    }else{
      return false;
    }
  }

  logout(){
    sessionStorage.removeItem("username");
    sessionStorage.removeItem("basicauth");
    sessionStorage.removeItem("userData");
    this._userMessageSource.next(sessionStorage.getItem("username"));
  }

  addRegisteredUser(user:User){
    this.users.push(user);
    console.log("Pushed value to pool: "+ this.users);
  }
  findUser(email:string){
    for(let i = 0; i < this.users.length; i++){
      if(this.users[i].email === email){
        return this.users[i];
      }
    }
    return undefined;
  }
}
